package com.example.phonesmswebexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    final String TAG="PHONE-EXAMPLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void callPhoneButtonPressed(View view) {
        // Do not open the DIAL screen, just call the phone directly

        // 1. get the phone number from the user interface
        EditText etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        String number = etPhoneNumber.getText().toString();

        Log.d(TAG, "Phone number is: " + number);

        String formattedPhoneNumber = "tel:" + number;

        Log.d(TAG, "Formatted Phone number is: " + formattedPhoneNumber);

        // 4152341111 -NOT GOING TO WORK
        // tel:4152341111


        // 2. create an explicit intent
        //      Tell android that you want to open the default DIAL screen
        Intent i = new Intent(Intent.ACTION_CALL);
        // sending the phone number to the dial screen
        i.setData(Uri.parse(formattedPhoneNumber));

        // 3. run the intent
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivity(i);
        }
        else {
            Log.d(TAG, "ERROR: Cannot find app that matches ACTION_CALL intent");
        }
    }

    public void openDialScreenButtonPressed(View view) {

        // 1. get the phone number from the user interface
        EditText etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        String number = etPhoneNumber.getText().toString();

        Log.d(TAG, "Phone number is: " + number);

        String formattedPhoneNumber = "tel:" + number;

        Log.d(TAG, "Formatted Phone number is: " + formattedPhoneNumber);

        // 4152341111 -NOT GOING TO WORK
        // tel:4152341111


        // 2. create an explicit intent
        //      Tell android that you want to open the default DIAL screen
        Intent i = new Intent(Intent.ACTION_DIAL);
        // sending the phone number to the dial screen
        i.setData(Uri.parse(formattedPhoneNumber));

        // 3. run the intent
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivity(i);
        }
        else {
            Log.d(TAG, "ERROR: Cannot find app that matches ACTION_DIALER intent");
        }


    }

    public void openSMSScreenButtonPressed(View view) {
        // 1. get phone number
        EditText etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        String number = etPhoneNumber.getText().toString();
        // make a formatted phone number
        String formattedPhoneNumber = "smsto:" + number;

        // 2. get the message
        EditText etMessage = (EditText) findViewById(R.id.etMessage);
        String message = etMessage.getText().toString();

        // 3. build the intent
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse(formattedPhoneNumber));
        i.putExtra("sms_body", message);


        // 4. open the app that is associated with the intent
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivity(i);
        }
        else {
            Log.d(TAG, "ERROR: Cannot find app that matches ACTION_SENDTO intent");
        }

    }

    public void sendSMSButtonPressed(View view) {
        // sending an sms directly

        // 1. get the phone number
        // 2. get the message

        EditText etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        String number = etPhoneNumber.getText().toString();

        //String formattedPhoneNumber = "smsto:" + number; //NOT REQUIRED

        // 2. get the message
        EditText etMessage = (EditText) findViewById(R.id.etMessage);
        String message = etMessage.getText().toString();

        // ----

        // 3. do some SMS configuration with your cell phone carrier
        //        - Configure the SMSC (Short Message Service Center)
        String scAddress = null;

        // 4. Create some required intent variables
        PendingIntent sentIntent = null;
        PendingIntent deliveryIntent = null;

        // 5. Send the SMS using SMSManager (built in Android class)
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(number, scAddress, message, sentIntent, deliveryIntent);

        smsManager.sendTextMessage("4162223333", scAddress, "hello", sentIntent, deliveryIntent);


    }
    public void openWebpageButtonPressed(View view) {

    }



}
